covenant-kbx (0.6-0kali7) kali-dev; urgency=medium

  * Do not publish port if using host network

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 06 Jan 2023 13:42:26 +0700

covenant-kbx (0.6-0kali6) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Update lintian override info to new format on line 1, 2, 3, 4, 5, 6, 7, 8, 9, 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

  [ Arnaud Rebillout ]
  * Use the host network

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 05 Jan 2023 13:52:57 +0700

covenant-kbx (0.6-0kali5) kali-dev; urgency=medium

  * Fix FTBFS "Unable to find package DonutCore"

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 20 May 2021 15:10:39 +0700

covenant-kbx (0.6-0kali4) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Disable i386 build on CI

  [ Arnaud Rebillout ]
  * Rebuild with latest kaboxer to add cli helpers

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 11 May 2021 15:50:02 +0700

covenant-kbx (0.6-0kali3) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Remove obsolete comment in debian/watch
  * Fix upgrade from v0.5 to v0.6
  * Bump Standards-Version to 4.5.1 (no changes)
  * Install the upstream icon
  * Drop i386

  [ Arnaud Rebillout ]
  * Drop i386 architecture for now
  * Revert "Drop i386 architecture for now"

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 22 Apr 2021 09:52:16 +0200

covenant-kbx (0.6-0kali2) kali-dev; urgency=medium

  * Update registry details

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 13 Apr 2021 21:13:19 +0700

covenant-kbx (0.6-0kali1) kali-dev; urgency=medium

  * Use custom script for uscan
  * New upstream version 0.6
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 12 Apr 2021 14:02:18 +0200

covenant-kbx (0.5-0kali2) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Strip unusual field spacing from debian/control.

  [ Raphaël Hertzog ]
  * Integrate covenant in the Kali menu
  * Add Depends on xdg-utils for xdg-open that we use
  * Use kaboxer hook scripts instead of our own wrapper

 -- Raphaël Hertzog <raphael@offensive-security.com>  Wed, 09 Dec 2020 16:08:45 +0100

covenant-kbx (0.5-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Initial release

  [ Raphaël Hertzog ]
  * Try to build a kaboxer container from Gitlab CI
  * Change kaboxer.yaml to fetch image from gitlab registry
  * dh_kaboxer no longer generates a tarball by default
  * Use data.tar.gz instead of data.orig to be more explicit about the format

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Jul 2020 15:34:55 +0200
